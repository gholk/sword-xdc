var webxdc = {
    sendUpdate(u, descr) {
        // nop and no receive update
        this.warn('sword-xdc will not send anything')
    },
    async setUpdateListener(cb, serial) {
        this.warn('sword-xdc will receive anything')
    },
    async sendToChat(message) {
        this.notSupport()
    },
    notSupport() {
        throw new Error('no support')
    },
    warn() {
        console.warn(...arguments)
    },
    async importFiles(filter) {
        this.notSupport()
    },
    get selfAddr() {
        return window.location.href
    },
    get selfName() {
        return 'user@sword-xdc'
    }
}
