class MessagePoster {
    constructor() {
        return this._constructor(...arguments)
    }
    _constructor(worker) {
        this.worker = worker
        this.seqList = []
        this.messageHandlerBind = this.messageHandler.bind(this)
        worker.addEventListener('message', this.messageHandlerBind)
    }
    destroy() {
        this.worker.removeEventListener('message', this.messageHandlerBind)
    }
    messageHandler(e) {
        const {message, id} = e.data
        this.seqList[id].resolve(message)
        this.seqList[id] = null
    }
    async send(message, w = this.worker) {
        const id = this.seqGen()
        const defer = this.seqList[id] = new Defer()
        const pack = {message, id}
        if (w == navigator.serviceWorker) w.controller.postMessage(pack)
        else w.postMessage(pack)
        return await defer.promise
    }
    seqGen() {
        const l = this.seqList
        for (let i=0; i<l.length; i++) {
            if (!l[i]) return i
        }
        return l.push(null)-1
    }
    static get global() {
        return self
    }
    static reply(event, message) {
        const client = event.source || this.global
        const {id} = event.data
        client.postMessage({id, message})
    }
    static replyRegist(target, cb) {
        target.onmessage = event => {
            const req = event.data.message
            const response = cb(req)
            if (response && response.then) {
                response.then(res => this.reply(event, res))
            }
            else this.swReply(event, response)
        }
    }
}

class Defer {
    constructor() {
        this.promise = new Promise((ok, error) => {
            this.resolve = ok
            this.reject = error
        })
    }
}

function sleep(s) {
    return new Promise(wake => setTimeout(wake, s*1000))
}
