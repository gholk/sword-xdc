// service worker

// import './node_modules/jszip/dist/jszip.min.js'
// import * as idb from './node_modules/idb/build/index.js'
importScripts('./node_modules/jszip/dist/jszip.min.js')
importScripts('./node_modules/idb/build/umd-with-async-ittr.js')
importScripts('./lib.js')

self.addEventListener('activate', e =>
    e.waitUntil(self.clients.claim())
)

const xdcr = {
    async list() {
        const db = await this.dbConnect()
        const tx = db.transaction(this.storeName)
        const list = []
        for await (const p of tx.store) {
            list.push(Object.assign({}, p.value, {file: null}))
        }
        await tx.done
        return list
    },
    async add(m) {
        const {file} = m
        const timeAdd = Date.now()
        const id = await this.idGen(file)
        const timeModified = file.lastModified
        const name = file.name
        await this.addToDb({file, name, id, timeModified, timeAdd}, id)
        return id
    },
    async idGen(blob) {
        const buf = await blob.arrayBuffer()
        const hash = await crypto.subtle.digest('SHA-256', buf)
        const u8 = new Uint8Array(hash)
        let s = ''
        for (const u of u8) s += ('0' + u.toString(16)).slice(-2)
        return s
    },
    async addToDb(x, key) {
        const db = await this.dbConnect()
        await db.add(this.storeName, x, key)
        this.cache.set(key, x)
    },
    async getFromDb(key) {
        const c = this.cache
        if (c.has(key)) return c.get(key)
        const db = await this.dbConnect()
        return await this.db.get(this.storeName, key)
    },
    cache: {
        map: new Map(),
        interval: 15 * 60, // 15m
        timeout: 10,
        async touch(id, second = this.interval) {
            const o = this.map.get(id)
            o.time += second
            await sleep(second)
            o.time -= second
            if (o.time <= this.timeout) {
                this.map.delete(id)
            }
        },
        has(id) {
            return this.map.has(id)
        },
        get(id) {
            const m = this.map
            let o = null
            if (m.has(id)) {
                o = m.get(id).value
                this.touch(id)
            }
            return o
        },
        set(id, x) {
            const m = this.map
            m.set(id, {time: 0, value: x})
            this.touch(id)
        }
    },
    async getFileInside(id, path) {
        const cache = this.cache
        let xdc = cache.get(id)
        if (!xdc) {
            xdc = await this.getFromDb(id)
            cache.set(id, xdc)
        }
        const zip = await JSZip.loadAsync(xdc.file)
        const zo = zip.file(path)
        if (!zo) return null
        const blob = await zo.async('blob')
        if (!blob.name) blob.name = path
        return blob
    },
    db: null,
    dbVersion: 2,
    dbName: 'webxdc-lib',
    async dbConnect() {
        if (!this.db) {
            const store = this.storeName
            this.db = await idb.openDB(this.dbName, this.dbVersion, {
                async upgrade(db, vold, vnew, tx, e) {
                    if (vold < 1) db.createObjectStore(store)
                    if (vold < 2) {
                        for await (const p of tx.objectStore(store)) {
                            const o = p.value
                            o.timeModified = o.file.lastModified
                            o.timeAdd = Date.now()
                            p.update(o)
                        }
                    }
                }
            })
        }
        return this.db
    },
    storeName: 'xdc-zip-file',
    async *storeConnect(option = {}) {
        if (!this.db) await this.dbConnect()
        const mode = option.mode || 'readwrite'
        const tx = this.db.transaction(this.storeName, mode)
        const store = tx.objectStore(this.storeName)
        yield store
        await tx.done
    }
}

// const mp = new MessagePoster(self)
const MP = MessagePoster
self.addEventListener('message', async e => {
    const {id, message} = e.data
    const m = message
    switch (m.type) {
    case 'xdc-add':
        const xid = await xdcr.add(m)
        return void await MP.reply(e, xid)
    case 'xdc-list':
        const list = await xdcr.list()
        return void await MP.reply(e, list)
    }
})

const path = {
    url: self.location,
    cwd: '',
    match(expect, x) {
        if  (x.startsWith(`${this.cwd}/${expect}`)) {
            return x.slice(`${this.cwd}/${expect}`.length)
        }
        else return null
    },
    asset(f) {
        return this.url.protocol + '//' + this.url.host + '/asset/' + f
    }
}

// fake/webxdc/by-id/%s/index.html
// fake/webxdc/%s/index.html
self.addEventListener('fetch', event => {
    const url = new URL(event.request.url)
    let sub
    let result
    if (sub = path.match('fake/eval/', url.pathname)) {
        result = eval(decodeURIComponent(sub))
    }
    else if (sub = path.match('fake/webxdc/by-id/', url.pathname)) {
        const id = sub.match(/^\w+/)[0]
        const subpath = sub.slice(id.length + 1)
        if (subpath == 'webxdc.js') {
            return event.respondWith(Response.redirect(
                path.asset(subpath), 301
            ))
        }
        result = xdcr.getFileInside(id, subpath)
    }
    else return
    event.respondWith(toResponse(result))
})

async function toResponse(x) {
    if (x && x.then) x = await x
    if (x == null) return new Response(null, {status: 404})
    let type = 'text/plain'
    if (x instanceof Object) {
        if (x.type) type = x.type
        else if (x.name) {
            switch (x.name.match(/\.(.{1,8}?$)/)[1].toLowerCase()) {
            case 'js':
                type = 'application/javascript'
                break
            case 'html':
                type = 'text/html'
                break
            case 'css':
                type = 'text/css'
                break
            }
        }
    }
    else x = String(x)
    const r = new Response(x, {headers: {'Contnet-Type': type}})
    return r
}

function html(h) {
    return new File([h], 'x.html', {type: 'text/html'})
}
function script(s) {
    return new File([s], 'x.js', {type: 'application/javascript'})
}
